<?php
	session_start();
	if(!isset($_COOKIE['email'])
	|| $_COOKIE['email'] == ""){
		header("Location: index.php");
		echo("---".$_COOKIE['email']."---");
	}

	$con = new mysqli("localhost","root","j","Gaa");
	$res = $con->query("SELECT visit FROM info");

	

?>

<!--
    sign in / sign up
    profile
	if their cookie isnt set, send them back to the index page
-->


<html>

	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
		<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
		<script src="lib/chartjs/Chart.js"></script>
	    	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


	</head>

	<body align="center">

		<!-- profile -->
		<div data-role="page" id="profile">
			<div data-role="header"><?php readFile('views/header.php'); ?></div>
			<div data-role="main">	<?php require('views/profile/profile.php')  ?></div>
			<div data-role="footer"><?php readFile('views/footer.html'); ?></div>

		</div>

		<!-- survey home page -->
		<div data-role="page" id="surveyHomepage">
			<div data-role="header"><?php readFile('views/header.php');  ?></div>
			<div data-role="main"><?php  require('views/survey/surveyHomepage.php'); ?></div>
		</div>

		<!-- My teams page -->
		<div data-role="page" id="myTeams">

			<div data-role="header"><?php readFile('views/header.php') ?></div>
			<div data-role="main"><?php require('views/teams/myTeams.php'); ?></div>
		</div>

		<!-- add a team -->
		<div data-role="page" id="addTeam">
			<div data-role="header"><?php readFile('views/header.php'); ?></div>
			<div data-role="main"><?php require('views/teams/addTeam.php'); ?></div>
		</div>

		<!-- edit profile -->
		<div data-role="page" id="editProfile">
			<div data-role="header"><?php readFile('views/header.php'); ?></div>
			<div data-role="main"><?php require('views/profile/editProfile.php'); ?></div>
		</div>

		<!-- chart view-->
		<div data-role="page" id="chartView">
			<div data-role="header"><?php readFile('views/header.php'); ?></div>
			<div data-role="main"><?php readFile('views/chart/chart.html'); readFile('views/chart/info.html') ?></div>
		</div>


		<!-- burnout info view-->
		<div data-role="page" id="info">
			<div data-role="header"><?php readFile('views/header.php'); ?></div>
			<div data-role="main"><?php readFile('views/info/info.html');?></div>
		</div>

	</body>

</html>
