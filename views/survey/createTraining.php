<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
		<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	</head>

	<body align="center">


		<div data-role="page">
		<form action="../../scripts/createTraining.php" method="post" data-ajax="false">
			<h1>Training</h1>


			<h3>Session Duration (hrs)</h3>
			<input type="text" name="duration">

			<h3>Personal Work Rate(1 = low)</h3>

			<label for="1r">1</label>
			<input type="radio" name="hardness" value="1" id="1r">
			<label for="2r">2</label>
			<input type="radio" name="hardness" value="2" id="2r">
			<label for="3r">3</label>
			<input type="radio" name="hardness" value="3" id="3r">
			<label for="4r">4</label>
			<input type="radio" name="hardness" value="4" id="4r">
			<label for="5r">5</label>
			<input type="radio" name="hardness" value="5" id="5r"><br>


			<h3>Session Type (best fit)</h3>
			<label for="1s">Light Training</label>
			<input type="radio" name="sessiontype" value="1" id="1s">
			<label for="2s">Standard Training</label>
			<input type="radio" name="sessiontype" value="2" id="2s">
			<label for="3s">Strength and Conditioning Training</label>
			<input type="radio" name="sessiontype" value="3" id="3s">
			<label for="4s">Match</label>
			<input type="radio" name="sessiontype" value="4" id="4s"><br>

			<h3>Select Team<h3>
			<?php
				// echo each team as a radio button
				$con = new mysqli("localhost","root","j","Gaa");
				$q = sprintf('SELECT teamName FROM teamPlayer WHERE playerEmail="%s"', $_COOKIE['email']);
				$res = $con->query($q);
				$i=0;
				while($row = mysqli_fetch_assoc($res)){
					$i++;

					$toEcho = sprintf('<label for="%dt">%s</label>	<input type="radio" name="team" value="%s" id="%dt">',$i, htmlspecialchars($row['teamName']), htmlspecialchars($row['teamName']), $i);
					echo($toEcho);
				}
			?>

			<br><br>
			<h3>Check your resting heart rate with this app: <a href="https://play.google.com/store/apps/details?id=si.modula.android.instantheartrate&hl=en" target="blank">Android</a> or <a href="https://itunes.apple.com/ie/app/instant-heart-rate-heart-rate/id409625068?mt=8" target="blank">iOS</a></h3>
			<p> Heart rate should be checked first thing in the morning and given in beats per minute (bpm)
			<input type="text" name="heartrate">
			<input type="submit" value="Submit">
		</form>
		</div>

	</body>


</html>
