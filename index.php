<!-- forward them to the profile if the cookie is set -->
<?php
	session_start();
	if(isset($_SESSION['email'])
	&& $_COOKIE['email'] != ""){
		header("Location: main.php");
	}
?>

<html>

	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
		<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	</head>

	<body align="center" >
		<!-- sign in -->
		<div data-role="page"  id="signInPage" >
			<div data-role="main"><?php require('views/signin/signInForm.php') ?></div>
		</div>

		<!-- Sign up -->
		<div data-role="page" id="signUp">
			<div data-role="main"><?php require('views/signin/signUpForm.php') ?></div>



		</div>

	</body>


</html>
